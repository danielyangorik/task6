<?php

namespace Drupal\kubform\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\UserData;

class MyForm extends FormBase {

  /**
   * Returns a unique string identifying the form.
   *
   * The returned ID should be a unique string that can be a valid PHP function
   * name, since it's used in hook implementation names such as
   * hook_form_FORM_ID_alter().
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'form-style-5';
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
//    $form['#attached']['css'][] = drupal_get_path('module', 'kubform') . '/css/style.css';
//    $form['#attached']['library'][] = 'kubform/my_form';
//    $form['#attached']['library'][] = 'uc_product/uc_product.styles';
    $form['name'] = [
      '#placeholder' => t('Имя'),
      '#type' => 'textfield'
    ];

    $form['email'] = [
      '#placeholder' => t('Почта'),
      '#type' => 'email'
    ];

    $form['bdate'] = [
      '#title' => t('Год рождения'),
      '#type' => 'select',
      '#options' => range(1950, 2020)
    ];

    $form['sex'] = [
      '#title' => t('Пол'),
      '#type' => 'radios',
      '#options' => array(t('Мужчина'), t('Женщина')),
    ];

    $form['limbs'] = [
      '#title' => t('Конечностей'),
      '#type' => 'radios',
      '#options' => range(1, 5)
    ];

    $form['power'] = [
      '#title' => t('Спсобности'),
      '#type' => 'select',
      '#options' => array(t('бессмертие'), t('прохождениесквозь стены'), t('левитация')),
      '#multiple' => TRUE
    ];

    $form['bio'] = [
      '#title' => t('Биография'),
      '#type' => 'textarea'
    ];

    $form['contr'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('С контрактом ознакомлен')
    ];

    $form['submit'] = [
      "#type" => "submit",
      '#value' => $this->t('Отправить'),
    ];

    return $form;

  }

  public function submitForm(array &$form, FormStateInterface $form_state) {

    $module = 'kubform';
    $key = 'form';
    $to = \Drupal\user\Entity\User::load(1)->getEmail();
    $langcode = "ru";
    $mailManager = \Drupal::service('plugin.manager.mail');
    $params = [];
    $params['message'] =serialize($form_state->getUserInput());
    $send = true;
    $result = $mailManager->mail($module, $key, NULL, $langcode, $params, NULL, $send);
    if ($result['result'] !== true) {
      $message = t('There was a problem sending your email notification');
      drupal_set_message($message, 'error');
      \Drupal::logger('kubform')->error($message);
    } else {
      $message = t('An email notification has been sent');
      drupal_set_message($message, 'status');
      \Drupal::logger('kubform')->notice($message);
      \Drupal::logger('kubform')->notice($params['message']);
    }

  }

}
